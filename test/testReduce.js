const items = require('../numbers')
const reduce = require('../reduce')

function sumReducer(initialValue, current) {
    return initialValue + current;
}

let startVal=0
if(startVal=='undefined'){
    startVal=items[0]
}else{
    startVal=0
}

const sumOfNumbers= reduce(items, sumReducer, startVal)

console.log("Sum of array of elements " + sumOfNumbers);