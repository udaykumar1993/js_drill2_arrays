const flatten = require('../flatten')
const nestedArray = [1, [2], [[3]], [[[4]]]];

let result = flatten(nestedArray)
if (result.length > 0) {
    console.log(`test case is Passed : After flatten the nested Array is ${result}`)
} else {
    console.log(`test case is Failed : there is no values in the given array`)
}


