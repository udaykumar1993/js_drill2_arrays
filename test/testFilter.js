const items = require('../numbers')
const filter = require('../filter')

function evenValues(element) {
    if (element % 2 == 0) {
        return true
    }
}

let filteredValues = filter(items, evenValues)
if (filteredValues.length > 0) {

    console.log(`test case is Passed :the even values are ${filteredValues}`)
} else {
    console.log(`test case is Failed : there is no even values  in array`)
}