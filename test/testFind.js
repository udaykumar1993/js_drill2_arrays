const items = require('../numbers.js')
const find = require('../find.js')

let findNumber = 5;
function callBack(number) {
    if (number == findNumber) {
        return true
    }
}

let result = find(items, callBack)

if (result) {
    console.log(`Test case is Passed : Number ${findNumber} is found in the array`)
} else {
    console.log(`Test case is Failed : Number ${findNumber} is not found in the array`)
}