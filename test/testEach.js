const items = require('../numbers')
const each = require('../each')

function callback(element, index) {
    console.log(`array element ${element} index is ${index}`);
}


each(items, callback);