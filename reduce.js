function reduce(items, reducer, initialValue) {
    let value = initialValue;
    for (let index = 0; index < items.length; index++) {
        value = reducer(value, items[index])
    }
    return value;
}

module.exports=reduce;

