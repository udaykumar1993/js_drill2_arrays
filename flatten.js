function flatten(nestedArray) {
    let fltArray = []
    for (let index = 0; index < nestedArray.length; index++) {
        if (Array.isArray(nestedArray[index])) {
            fltArray = fltArray.concat(flatten(nestedArray[index]));
        } else {
            fltArray.push(nestedArray[index]);
        }
    }
    return fltArray;
}

module.exports = flatten;

