const items = require('./numbers')

function find(elements, callBack) {
    for (let index = 0; index < elements.length; index++) {
        if (callBack(elements[index])) {
            return true
        }
    }
    return false
}

module.exports = find


