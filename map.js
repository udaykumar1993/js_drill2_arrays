function map(array, doubleArray) {
    var newArray = [];
    for (let index = 0; index < array.length; index++) {
        newArray.push(doubleArray(array[index]))
    }
    return newArray
}

module.exports = map;
