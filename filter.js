function filter(elements, filterValues) {
    let array = []
    for (let index = 0; index < elements.length; index++) {
        if (filterValues(elements[index])) {
            array.push(elements[index])
        }
    }
    return array
}

module.exports = filter;